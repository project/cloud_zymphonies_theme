# Cloud Zymphonies Theme

### INTRODUCTION

Cloud Zymphonies Theme is our Mobile-first Drupal 8 responsive theme. 
This theme features a custom sideshow, responsive layout, 
multiple column layouts and is highly customizable. 
It also supports Google Fonts and it is great for any kind of business website.

Cloud Zymphonies Theme is developed with all latest technologies 
Drupal 8, Bootstrap v4 and Font Awesome v5 etc.

### REQUIREMENTS

This Theme is independent theme and it doesn't required any core module.

### INSTALLATION

Just download theme and enable the theme.

### CONFIGURATION

Create bocks and assign to respective blocks.

### Features

- Drupal 8 core.
- Bootstrap v4.
- Font Awesome v5.
- Mobile-first responsive theme.
- Custom menu bar.
- Light weight theme.
- Fully responsive design.
- Quick informations in header.
  - User login details.
  - Social media links.
- Included Sass & Compass source file.
  - Colors are stored in Sass variable.
  - Well organized Sass code.
- Custom Sideshow - Unlimited image upload.

### Demo

Visit the project's 
[Live Demo](http://demo.zymphonies.com/free-theme/d8/cloud-zymphonies-theme/)

[Read More]: http://www.zymphonies.com/drupal-free-theme/cloud-zymphonies-theme
