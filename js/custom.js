/*
 * Filename:     custom.js
 * Version:      1.0.0 (2017-02-12)
 * Website:      http://www.zymphonies.com
 * Description:  Global Script
 * Author:       Zymphonies Team
                info@zymphonies.com
 */

function theme_menu() {

  'use strict';

  // Mobile menu toggle.
  jQuery('.navbar-toggle').click(function () {
    jQuery('.region-primary-menu').slideToggle();
  });

  // Mobile dropdown menu.
  if (jQuery(window).width() < 767) {
    jQuery('.region-primary-menu li a:not(.has-submenu)').click(function () {
      jQuery('.region-primary-menu').hide();
    });
  }
}

jQuery(document).ready(function ($) {

  'use strict';

  theme_menu();
});

